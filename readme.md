# Binar Academy X Kampus Merdeka

Team 5 (Car Web Application using React)

FrontEnd : React.js <br />
BackEnd : Express.js (Design Pattern: Service Repository Pattern)

- Yehezkiel Victorious Ermanto
- Muhammad Arif Saputra
- Vito Raditya Fauzan
- Nur Zidan Haq

---

- FrontEnd Design: Arif & Zidan
- ERD, Google Auth, Filter Car: Yehezkiel
- Login & Register user, check current user, authorization : Vito

---

![ERD Database](./car-web-application.jpg)
